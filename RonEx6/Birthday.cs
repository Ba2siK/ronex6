﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using static RonEx6.SignIn;

namespace RonEx6
{
    public partial class Birthday : Form
    {       
        private Dictionary <string,string> birthdaysNnames;

        public Birthday(string userBDfileInfo)
        {         
            birthdaysNnames = GetDic(userBDfileInfo , 1 , 0);
            InitializeComponent();
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            CultureInfo ci = CultureInfo.CreateSpecificCulture("en-GB"); // english cult'
            string date = monthCalendar1.SelectionRange.Start.ToShortDateString(); // getting the date from the calendar

            date = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture) // converting to wanted format
                        .ToString("M/d/yyyy", CultureInfo.InvariantCulture);

            if (birthdaysNnames.ContainsKey(date))   // if the date exists in the dic       
                bdTxt.Text = @"! חוגג יום הולדת " + birthdaysNnames[date] + @" - בתאריך הנבחר "; // printing the name of the person
            
            else           
                bdTxt.Text = @"בתאריך הנבחר – אף אחד לא חוגג יום הולדת";
            
        }
    }
}
