﻿namespace RonEx6
{
    partial class SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExt = new System.Windows.Forms.Button();
            this.btnEntr = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.Label();
            this.usrnmField = new System.Windows.Forms.TextBox();
            this.passField = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnExt
            // 
            this.btnExt.Location = new System.Drawing.Point(42, 135);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(86, 32);
            this.btnExt.TabIndex = 0;
            this.btnExt.Text = "Exit";
            this.btnExt.UseVisualStyleBackColor = true;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // btnEntr
            // 
            this.btnEntr.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnEntr.Location = new System.Drawing.Point(158, 135);
            this.btnEntr.Name = "btnEntr";
            this.btnEntr.Size = new System.Drawing.Size(86, 32);
            this.btnEntr.TabIndex = 1;
            this.btnEntr.Text = "Enter";
            this.btnEntr.UseVisualStyleBackColor = true;
            this.btnEntr.Click += new System.EventHandler(this.btnEntr_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Yellow;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username:";
            // 
            // txtPass
            // 
            this.txtPass.AutoSize = true;
            this.txtPass.BackColor = System.Drawing.Color.Red;
            this.txtPass.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.Location = new System.Drawing.Point(33, 80);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(105, 20);
            this.txtPass.TabIndex = 3;
            this.txtPass.Text = "Password:";
            // 
            // usrnmField
            // 
            this.usrnmField.Location = new System.Drawing.Point(144, 28);
            this.usrnmField.Name = "usrnmField";
            this.usrnmField.Size = new System.Drawing.Size(100, 20);
            this.usrnmField.TabIndex = 4;

            // 
            // passField
            // 
            this.passField.Location = new System.Drawing.Point(144, 80);
            this.passField.Name = "passField";
            this.passField.PasswordChar = '*';
            this.passField.Size = new System.Drawing.Size(100, 20);
            this.passField.TabIndex = 5;
            // 
            // SignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 191);
            this.Controls.Add(this.passField);
            this.Controls.Add(this.usrnmField);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEntr);
            this.Controls.Add(this.btnExt);
            this.Name = "SignIn";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.Button btnEntr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txtPass;
        private System.Windows.Forms.TextBox usrnmField;
        private System.Windows.Forms.TextBox passField;
    }
}

