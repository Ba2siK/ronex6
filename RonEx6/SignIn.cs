﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RonEx6
{
    public partial class SignIn : Form
    {
        private Dictionary<string, string> UsernamesNpasswords;

        public SignIn()
        {
            UsernamesNpasswords = GetDic("Users.txt" , 0 , 1);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            Application.Exit(); // closing
        }

        private void btnEntr_Click(object sender, EventArgs e)
        {
            if (UsernamesNpasswords.ContainsKey(usrnmField.Text) && UsernamesNpasswords[usrnmField.Text] == passField.Text)
            {
                this.Hide();
                Birthday wnd = new Birthday(usrnmField.Text + "BD.txt"); // opening the new window and sending the bd filename.
                wnd.ShowDialog();

                Application.Exit(); // closing
                     
            }
            else          
                MessageBox.Show("Sorry. Wrong username or password\n");
                    
        }

        /* First and second option decides whether the list will be determined according to what is before the comma or what follows it */
        public static Dictionary<string, string> GetDic(string filename , int firstOption , int secondOption)
        {
            string line;
            Dictionary<string, string> myDic = new Dictionary<string, string>(); // dic of date,name
            System.IO.StreamReader sr = new System.IO.StreamReader(filename); // opening file for reading 

            while ((line = sr.ReadLine()) != null) // reading by line
                myDic.Add((line.Split(','))[firstOption], (line.Split(','))[secondOption]); // adding to dic the date,name

            sr.Close();

            return myDic;
        }
    }
}
